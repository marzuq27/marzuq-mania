<?php
session_start();
if (!isset($_SESSION['username'])) {
  header("Location: login.html");
}
?>
<!DOCTYPE html>
<html>

<head>
  <title>Upload Jurnal</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="Home.css">
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
      <h1 style="text-align: left  ;  ">WELCOME TO EDUCORNER</h1>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="index.php">Home</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Profile
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="profile.php">Profile <?php echo $_SESSION['username']; ?></a>
            <a class="dropdown-item" href="logout.php">Logout</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="uploadjurnal.php">Upload</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="container ">
    <h2>Journal</h2>
    <br>
    <form method="post" enctype="multipart/form-data" action="upload.php">
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Title</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="title" placeholder="Enter Title">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 col-form-label">Author</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="author" placeholder="Enter Author">
        </div>
      </div>
      <div class="form-group">
        <label>File Jurnal</label>
        <input type="file" class="form-control-file" name="file">
      </div>
      <div class="form-group row">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary">Upload</button>
        </div>
      </div>
    </form>
  </div>
  <div class="container mt-3">
    <div class="middle">
      <img src="background2.jpg" style="width: 1080px; height: 700px;">
    </div>
    <div class="footer" style="background-color: white; width: 1400px; height: 70px; ">
      <img src="educorner.png" style="float: left; width: 300px;">
    </div>


</body>

</html>