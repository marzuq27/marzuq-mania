<?php
session_start();
if(!isset($_SESSION['username']))     
 {
	 header("Location: login.html");
 }
?>

<head>
  <title>Edit Profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="Home.css">
</head>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
    <h1 style="text-align: left  ;  ">WELCOME TO EDUCORNER</h1>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Home</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Profile
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="profile.php">Profile <?php echo $_SESSION['username']; ?></a>
          <a class="dropdown-item" href="logout.php">Logout</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="uploadjurnal.php">Upload</a>
      </li>
    </ul>
  </div>
</nav>
<hr>
<div class="container bootstrap snippet">
  <div class="row">
    <div class="col-sm-10">
      <h1><?= $_SESSION['username']; ?></h1>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-3">
      <?php
      $user = $_SESSION['username'];
      include 'database.php';
      $data = mysqli_query($db, "select * from users where username = '$user' ");
      while ($d = mysqli_fetch_array($data)) {
        $avatar = $d['avatar'];
        ?>
        <?php

          if (empty($d['avatar'])) {
            ?>
          <div class="text-center">
            <img src="http://ssl.gstatic.com/accounts/ui/avatar_2x.png" style="width:250px;" class="avatar img-circle img-thumbnail" alt="avatar">
          </div>

        <?php
          } else { ?>
          <div class="text-center">
            <img src="images/avatar/<?= $avatar; ?>" style="width:250px; height:250px;" class="rounded" alt="avatar">
          </div>
        <?php }
          ?>

    </div>
    <!--/col-3-->
    <div class="col-sm-9">
      <div class="tab-content">
        <div class="tab-pane active" id="home">
          <hr>
          <form class="form" action="edit_profile.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <div class="col-xs-6">
                <label for="first_name">
                  <h4>Nama</h4>
                </label>
                <input value="<?php echo $d['nama']; ?>" type="text" class="form-control" name="nama" placeholder="Isi Nama" title="enter your name" required>
              </div>
            </div>
            <div class="form-group">

              <div class="col-xs-6">
                <label for="last_name">
                  <h4>No HP</h4>
                </label>
                <input value="<?php echo $d['no_hp']; ?>" type="number" class="form-control" name="no_hp" title="enter your handphone " required>
              </div>
            </div>

            <div class="form-group">

              <div class="col-xs-6">
                <label for="email">
                  <h4>Email</h4>
                </label>
                <input value="<?php echo $d['email']; ?>" type="email" class="form-control" name="email" placeholder="you@email.com" title="enter your email." required>
              </div>
            </div>
            <div class="form-group">

              <div class="col-xs-6">
                <label>
                  <h4>Lokasi</h4>
                </label>
                <input value="<?php echo $d['lokasi']; ?>" type="text" class="form-control" name="lokasi" placeholder="lokasi" required>
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-6">
                <label>
                  <h4>Upload Gambar</h4>
                </label>
                <input name="file" type="file" class="form-control-file" required>
              </div>
            </div>
            <!--    <div class="form-group">
                          <div class="col-xs-6">
                              <label for="password"><h4>Password Baru</h4></label>
                              <input type="password" class="form-control" name="password" placeholder="password" title="enter your password." required>
                          </div>
                      </div> -->
            <div class="form-group">
              <div class="col-xs-12">
                <br>
                <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Save</button>
                <a href="profile.php" class="btn btn-lg"> <i class="glyphicon glyphicon-home"></i> Cancel</a>
              </div>
            </div>
          </form>
        <?php
        }
        ?>
        <hr>
        </div>
        <!--/tab-pane-->
      </div>
      <!--/tab-content-->
    </div>
    <!--/col-9-->
  </div>
  <!--/row-->