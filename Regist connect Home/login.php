<?php include('server.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>registrasi form Educorner</title>
	<link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
	<div class="header">
		<h2>Login Educorner</h2>
	</div>

	<form method="post" action="login.php">
		<?php include('errors.php'); ?>
		<div class="input-group">
			<label>Username</label>
			<input type="text" name="username" value="<?php echo $username; ?> ">
		</div>
		
		<div class="input-group">
			<label>Password</label>
			<input type="text" name="password">
		</div>
		
		<div class="input-group">
			<button type="submit" name="register" class="btn">Login</button>
		</div>
		<p>
			Not Have a member? <a href="regist.php">Sign Up</a>
		</p>
	</form>

</body>
</html>