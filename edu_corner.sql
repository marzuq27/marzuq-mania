-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2019 at 11:45 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edu_corner`
--

-- --------------------------------------------------------

--
-- Table structure for table `jurnal`
--

CREATE TABLE `jurnal` (
  `id` int(11) NOT NULL,
  `nama_jurnal` varchar(200) NOT NULL,
  `file_jurnal` varchar(256) NOT NULL,
  `id_users` int(11) NOT NULL,
  `nama_pengupload` varchar(100) NOT NULL,
  `pengarang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal`
--

INSERT INTO `jurnal` (`id`, `nama_jurnal`, `file_jurnal`, `id_users`, `nama_pengupload`, `pengarang`) VALUES
(2, 'buku saku', 'buku saku 191023 ver 2 revisi dr andy.pdf', 2, 'rananta', 'AIrlangga'),
(5, 'Manajemen Audit', 'jpmanajemendd161434.pdf', 1, 'marzuq', 'Albert'),
(6, 'Ono', 'SI4008-8.pdf', 0, 'nopal', 'Anthony'),
(8, 'Modul 2 HR', 'MODUL 2 HR.pdf', 6, 'imak', 'Lab HR'),
(9, 'PENGARUH BUDAYA ORGANISASI, DISIPLIN KERJA, DA', 'jpmanajemendd161432.pdf', 6, 'imak', 'Anggy'),
(10, 'Test Upload', 'Studi Kasus WAD - Database CRUD & Session.pdf', 7, 'Aji', 'Test Author');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `avatar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `nama`, `lokasi`, `no_hp`, `avatar`) VALUES
(1, 'marzuq', 'marzuq@marzuq.com', '202cb962ac59075b964b07152d234b70', 'Marzuq Rabbani', 'Bandung', '085294880117', 'marzuq.jpg'),
(2, 'rananta', 'rananta@rananta.com', '202cb962ac59075b964b07152d234b70', 'Rananta', 'Tangerang', '13.1531', 'rananta.jpg'),
(3, 'nopal', 'nopal@gmail.com', '3def184ad8f4755ff269862ea77393dd', 'Nopal', 'Nopal', '0852948015', 'nopal.jpg'),
(4, 'bintang', 'bintangbh@gmail.com', '202cb962ac59075b964b07152d234b70', 'Bintang BH', 'Jakarta', '081', 'sunmi.jpg'),
(5, 'isam', 'isam@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', '', '', ''),
(6, 'imak', 'imak@gmail.com', '202cb962ac59075b964b07152d234b70', 'Imakk', 'Jakarta', '021', 'wp3079570.jpg'),
(7, 'Aji', 'aji@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', ''),
(8, 'Aji', 'aji@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', ''),
(9, 'Aji', 'aji@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', ''),
(10, 'Aji', 'aji@gmail.com', '202cb962ac59075b964b07152d234b70', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jurnal`
--
ALTER TABLE `jurnal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
